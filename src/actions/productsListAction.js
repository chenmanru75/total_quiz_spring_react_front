export const getProductsList = () => (dispatch) => {
  fetch(`http://localhost:8080/api/products`)
    .then(response => response.json())
    .then(data => {
      dispatch({
        type: 'PRODUCTS_LIST',
        products_list: data
      })
    })
};