const initState = {
  products_list:[]
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'PRODUCTS_LIST':
      return {
        ...state,
        products_list: action.products_list
      };
    default:
      return state
  }
};
