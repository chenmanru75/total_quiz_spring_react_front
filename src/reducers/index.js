import {combineReducers} from "redux";
import productsListReducers from "./productsListReducers";

const reducers = combineReducers({
  products_list:productsListReducers
});
export default reducers;