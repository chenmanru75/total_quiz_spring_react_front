import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProductsList} from '../actions/productsListAction';
import './Home.less'
class Home extends Component {

  constructor(props, context) {
    super(props, context);
    this.handleAddProduct = this.handleAddProduct.bind(this);
  }

  componentDidMount() {
    this.props.getProductsList();
  }

  handleAddProduct(id){
    console.log(id);
    fetch(`http://localhost:8080/api/addproduct/${id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    })
      .then(response => {
        if (response.status === 200) {
          alert('添加成功！');
        }else{
          alert('添加失败，对不起服务器没写好！');
        }
      })
  }

  render() {
    const {productsList} = this.props;
    return (

        <ul className="home-page">
          {
            productsList.map((product,index) => (
              <li key={index}>
                <div><img src={product.url} width="100px"/></div>
                <h2>{product.name}</h2>
                <p>单价：<span>{product.price}</span>元/<span>{product.unit}</span></p>
                <button onClick = {() => this.handleAddProduct(product.id)}>添加</button>
              </li>
            ))
          }
        </ul>

    );
  }
}

const mapStateToProps = (state) => ({
  productsList: state.products_list.products_list
});

const mapDispatchToProps = (dispatch) => ({
  getProductsList: () => (dispatch(getProductsList()))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);