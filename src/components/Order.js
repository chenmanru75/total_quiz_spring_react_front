import React, {Component} from 'react';

class Order extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      orderProduct: [],
      deleteProductId: ''
    };
    this.handleDeleteOrder = this.handleDeleteOrder.bind(this);
  }

  componentDidMount() {
    fetch(`http://localhost:8080/api/orders`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    })
      .then(response => response.json())
      .then(data => {
        this.setState({
          orderProduct: data
        })
      })
  }

  handleDeleteOrder(id) {
    if (confirm("确定删除该商品吗？")) {
      fetch(`http://localhost:8080/api/deleteproduct/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json;charset=UTF-8'
        }
      })
        .then(response => {
          if (response.status === 200) {
            alert('删除成功！');
            window.location.reload();
          }
        })
    }
  }

  render() {
    const {orderProduct} = this.state;
    return (
      <div>
        <ul>
          {
            orderProduct.length === 0 ? "暂无订单" : orderProduct.map((product, index) => (
              <li key={index}>
                <p>名字：<span>{product.name}</span></p>
                <p>单价：<span>{product.price}</span></p>
                <p>数量：<span>{product.size}</span></p>
                <p>单位：<span>{product.unit}</span></p>
                <p>
                  <button onClick={() => this.handleDeleteOrder(product.product_id)}>删除</button>
                </p>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

export default Order;