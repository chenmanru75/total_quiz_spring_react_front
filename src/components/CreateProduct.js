import React, {Component} from 'react';

class CreateProduct extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      price: '',
      unit: '',
      url: ''
    };
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleChangeUnit = this.handleChangeUnit.bind(this);
    this.handleChangeUrl = this.handleChangeUrl.bind(this);
    this.handleCreateProduct = this.handleCreateProduct.bind(this);
  }

  handleChangeName(e) {
    this.setState({
      name: e.target.value
    })
  }

  handleChangePrice(e) {
    this.setState({
      price: e.target.value
    })
  }

  handleChangeUnit(e) {
    this.setState({
      unit: e.target.value
    })
  }

  handleChangeUrl(e) {
    this.setState({
      url: e.target.value
    })
  }

  handleCreateProduct() {
    const {name, price, unit, url} = this.state;
    if (confirm("确定创建该商品吗？")) {
      fetch(`http://localhost:8080/api/products`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({
          name: name,
          unit: unit,
          price: price,
          url: url
        })
      })
        .then(response => {
          if (response.status === 201) {
            alert('创建成功！');
            this.props.history.push('/');
          }
        })
    }
  }

  render() {
    return (
      <div>
        <h1>添加商品</h1>
        <label>
          <p>名称：</p>
          <input type="text" onChange={this.handleChangeName}/>
        </label>
        <label>
          <p>价格：</p>
          <input type="text" onChange={this.handleChangePrice}/>
        </label>
        <label>
          <p>单位：</p>
          <input type="text" onChange={this.handleChangeUnit}/>
        </label>
        <label>
          <p>图片：</p>
          <input placeholder="URL" type="text" onChange={this.handleChangeUrl}/>
        </label>
        <div>
          <button onClick={this.handleCreateProduct}>提交</button>
        </div>
      </div>
    );
  }
}

export default CreateProduct;