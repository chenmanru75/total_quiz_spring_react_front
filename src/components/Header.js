import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../style/header.less';

class Header extends Component {
  render() {
    return (
      <div className={'navWrap'}>
        <ul>
          <li><Link to="/">商城</Link></li>
          <li><Link to="/order">订单</Link></li>
          <li><Link to="/create">创建商品</Link></li>
        </ul>
      </div>
    );
  }
}

export default Header;