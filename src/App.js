import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./components/Home";
import CreateProduct from "./components/CreateProduct";
import Order from "./components/Order";
import Header from "./components/Header";

class App extends Component{
  render() {
    return (
      <div>
        <Router>
          <Header/>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/order" component={Order} />
            <Route exact path="/create" component={CreateProduct} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;